<?PHP

include_once ( "php/common.php" ) ;

$action = get_request ( 'action' , '' ) ;
$out = array ( 'status' => 'OK' , 'data' => array() ) ;

ini_set('user_agent','Magnus labs tools'); # Fake user agent
header('Content-type: application/json');
header("Connection: close");
header("Cache-Control: no-cache, must-revalidate");


if ( $action == 'items2site' ) {

	$db = openDB ( 'wikidata' ) ;
	$site = preg_replace ( '/[^a-z_-]/' , '' , get_request ( 'site' ) ) ;
	$items = array() ;
	foreach ( $_REQUEST['items'] AS $v ) $items[] = preg_replace ( '/\D/' , '' , $v ) ;
	if ( count ( $items ) > 0 ) {
		$sql = "SELECT ips_site_page,ips_item_id FROM wb_items_per_site WHERE ips_site_id='$site' AND ips_item_id IN (" . implode(',',$items) . ")" ;
//		$out['sql'] = $sql ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($row = $result->fetch_assoc()){
			$out['data'][] = array ( $row['ips_site_page'] , 'Q'.$row['ips_item_id'] ) ;
		}
	}

} else if ( $action == 'get_items_for_pages' ) {

	$db = openDB ( 'wikidata' ) ;
	$site = preg_replace ( '/[^a-z_-]/' , '' , get_request ( 'site' ) ) ;
	$titles = array() ;
	foreach ( $_REQUEST['titles'] AS $p ) {
		$titles[] = $db->real_escape_string ( str_replace ( '_' , ' ' , $p ) ) ;
	}
	
	if ( count ( $titles ) > 0 ) {
		$sql = "SELECT DISTINCT ips_item_id FROM wb_items_per_site WHERE ips_site_id='$site' AND ips_site_page IN (\"" . implode ( '","' , $titles ) . "\")" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($row = $result->fetch_assoc()){
			$out['data'][] = 'Q' . $row['ips_item_id'] ;
		}
	} ;


} else if ( $action == 'items_with_property' ) {


	$db = openDB ( 'wikidata' ) ;
	$prop = 'P' . preg_replace ( '/\D/' , '' , get_request ( 'prop' ) ) ;
	$items = array () ;
	foreach ( $_REQUEST['items'] AS $p ) {
		$items[] = 'Q' . preg_replace ( '/\D/' , '' , $p ) ;
	}

	if ( count ( $items ) > 0 ) {
		$sql = "SELECT DISTINCT page_title FROM page,pagelinks WHERE page_id=pl_from AND page_namespace=0 AND pl_namespace=120 AND pl_title='$prop' AND page_title IN ('" . implode ( "','" , $items ) . "');" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($row = $result->fetch_assoc()){
			$out['data'][] =  $row['page_title'] ;
		}
	}
	
} else if ( $action == 'items_with_item_link' ) {


	$db = openDB ( 'wikidata' ) ;
	$itemlink = 'Q' . preg_replace ( '/\D/' , '' , get_request ( 'itemlink' ) ) ;
	$items = array () ;
	foreach ( $_REQUEST['items'] AS $p ) {
		$items[] = 'Q' . preg_replace ( '/\D/' , '' , $p ) ;
	}

	if ( count ( $items ) > 0 ) {
		$sql = "SELECT DISTINCT page_title FROM page,pagelinks WHERE page_id=pl_from AND page_namespace=0 AND pl_namespace=0 AND pl_title='$itemlink' AND page_title IN ('" . implode ( "','" , $items ) . "');" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($row = $result->fetch_assoc()){
			$out['data'][] =  $row['page_title'] ;
		}
	}
	
} else if ( $action == 'get_item_data' ) {

	print '{"status":"OK","data":{' ;
	$first = true ;
	foreach ( $_REQUEST['items'] AS $k => $p ) {
		$item = 'Q' . preg_replace ( '/\D/' , '' , $p ) ;
		$url = "https://www.wikidata.org/wiki/Special:EntityData/$item.json" ;
		$c = file_get_contents ( $url ) ;
		if ( preg_match ( '/Invalid ID:/' , $c ) ) continue ;
		if ( !$first ) print ',' ;
		$first = false ;
		print '"' . $item . '":' ;
		print $c ;
	}
	print '}}' ;
	exit ( 0 ) ; // Passing through JSON data

} else if ( $action == 'items_no_site' ) {

	$db = openDB ( 'wikidata' ) ;
	$site = preg_replace ( '/[^a-z_-]/' , '' , get_request ( 'site' ) ) ;
	$items = array() ;
	foreach ( $_REQUEST['items'] AS $v ) $items[] = preg_replace ( '/\D/' , '' , $v ) ;
	if ( count ( $items ) > 0 ) {
		$sql = "SELECT DISTINCT ips_item_id FROM wb_items_per_site i1 WHERE i1.ips_item_id IN (" . implode(',',$items) . ") AND NOT EXISTS ( SELECT * FROM wb_items_per_site i2 WHERE i1.ips_item_id=i2.ips_item_id AND ips_site_id='$site' )" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($row = $result->fetch_assoc()){
			$out['data'][] = 'Q' . $row['ips_item_id'] ;
		}
	}


} else if ( $action == 'get_namespaces' ) {

	$server = get_request ( 'server' ) ;
	$url = "http://$server/w/api.php?action=query&meta=siteinfo&siprop=namespaces|namespacealiases&format=json" ;
	$d = file_get_contents ( $url ) ;
	if ( isset($d) and $d != '' ) {
		$d = json_decode ( $d ) ;
		$out['data'] = $d->query ;
	}

} else {
	$out['status'] = "Unknown action $action" ;
}

print json_encode ( $out ) ;

?>