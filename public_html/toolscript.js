/*
	ToolScript (c) 2014 by Magnus Manske
	released under GPLv2 or higher
*/

function ToolScriptResult ( ts__ , lang__ , proj__ ) {
	var self = this ;
	
	this.ts = ts__ ;
	this.pages = [] ;
	this.language = lang__ ;
	this.project = proj__ ;
	this.classname = 'ToolScriptResult' ;
	
	this.addPage = function ( title , ns ) {
		var trimmed_title = $.trim(title.replace(/_/g,' ')) ;
		if ( trimmed_title == '' ) return self ;
		var np = { } ;
		if ( typeof ns != 'undefined' ) {
			np.page_title = trimmed_title ;
			np.page_namespace = ns ;
		} else {
			np = self.splitNamespace ( trimmed_title.replace(/ /g,'_') ) ;
		}
		if ( self.hasPage ( np ) ) return self ; // Have that one already
		self.pages.push ( np ) ;
		return self ;
	}
	
	this.join = function ( list ) {
		var ret = self.ts.getNewList ( self.language , self.project ) ;
		if ( self.language != list.language || self.project != list.project ) {
			self.ts.logError ( "Trying to join a list from " + self.getSiteTitle() + " and " + list.getSiteTitle() ) ;
			return ret ;
		}
		
		if ( self.ts.verbose ) self.ts.log ( "Joining lists of " + self.pages.length + " and " + list.pages.length + " pages from " + self.getSiteTitle() + "..." ) ;
		
		$.each ( self.pages , function ( k , v ) {
			ret.pages.push ( $.extend ( {} , v ) ) ;
		} ) ;

		$.each ( list.pages , function ( k , v ) {
			if ( ret.hasPage ( v ) ) return ;
			ret.pages.push ( $.extend ( {} , v ) ) ;
		} ) ;
		
		if ( self.ts.verbose ) self.ts.log ( "joined into " + ret.pages.length + " unique pages" ) ;
		
//		self.ts.clog ( ret ) ;
		
		return ret ;
	}
	
	this.loadQuarryResult = function ( id , ns ) { // ns = default namespace; overwritten if JSON contains page_namespace
		$.ajax ( '//quarry.wmflabs.org/run/' + id + '/output/0/json' , {
			type : "GET" ,
			async : false ,
			dataType : 'json' ,
			complete : function ( dx , status ) {
				var d = JSON.parse ( dx.responseText ) ;
				var h2n = {} ;
				$.each ( (d.headers||[]) , function ( k , v ) { h2n[v] = k } ) ;
				if ( typeof h2n['page_title'] == 'undefined' ) {
					alert ( "Quarry result set #"+id+" does not contain page_title column, skipping" ) ;
					return ;
				}
				$.each ( (d.rows||[]) , function ( rowid , row ) {
					if ( typeof h2n['page_namespace'] != 'undefined' ) ns = row[h2n['page_namespace']] ;
					self.addPage ( row[h2n['page_title']] , ns ) ;
				} ) ;
			}
		} ) ;
		return self ;
	}
	
	this.loadWikidataInfo = function () {
		if ( self.language != '' || self.project != 'wikidata' ) {
			self.ts.logError ( "Can't load Wikidata info for " + self.getSiteTitle() + " pages" ) ;
			return ;
		}

		var items = [] ;
		$.each ( self.pages , function ( k , v ) {
			if ( v.page_namespace != 0 ) return ;
			if ( typeof v.wd != 'undefined' ) return ;
			items.push ( v.page_title ) ;
		} ) ;
		
		while ( items.length > 0 ) {

			var p = {
				action:'get_item_data' ,
				items:[]
			} ;
			
			while ( p.items.length < 1000 && items.length > 0 ) p.items.push ( items.pop() ) ;

			if ( self.ts.verbose ) self.ts.log ( "Getting Wikidata information for " + p.items.length + " items..." ) ;
			$.ajax ( self.ts.api.misc , {
				type : "POST" ,
				async : false ,
				data : p ,
				dataType : 'json' ,
				complete : function ( d , status ) {
					if ( status == 'success' ) {
						$.each ( self.pages , function ( k , v ) {
							var e = d.responseJSON.data[v.page_title] ;
							if ( typeof e == 'undefined' ) return ;
							if ( typeof ((e||{}).entities||{})[v.page_title] == 'undefined' ) return ;
							v.wd = e.entities[v.page_title] ;
						} ) ;
					} else {
//						self.ts.log ( status ) ;
//						self.ts.clog ( status ) ;
					}
				}
			} ) ;
		}
		
		if ( self.ts.verbose ) self.ts.log ( "Done." ) ;

		return self ;
	}

	this.hasPropertyOrItemLink = function ( key , val , cmd , keep ) {
		if ( self.language != '' || self.project != 'wikidata' ) {
			self.ts.logError ( "Can't check Wikidata statements for " + self.getSiteTitle() + " pages" ) ;
			return ;
		}
		
		var p = {
			action:cmd ,
			items:[]
		} ;
		p[key] = (val+'').replace(/\D/g,'') ,
		$.each ( self.pages , function ( k , v ) {
			if ( v.page_namespace == 0 ) p.items.push ( v.page_title ) ;
		} ) ;

		var ret = new ToolScriptResult ( self.ts , '' , 'wikidata' ) ;
		if ( self.ts.verbose ) self.ts.log ( "Checking Wikidata statements for " + p.items.length + " items..." ) ;
		$.ajax ( self.ts.api.misc , {
			type : "POST" ,
			async : false ,
			data : p ,
			dataType : 'json' ,
			complete : function ( d , status ) {
				if ( status == 'success' ) {
					var tmp = {} ;
					$.each ( d.responseJSON.data , function ( k , v ) {
						tmp[v] = true ;
					} ) ;
					
					$.each ( self.pages , function ( k , v ) {
						if ( ( keep && typeof tmp[v.page_title] != 'undefined' ) ||
							 ( !keep && typeof tmp[v.page_title] == 'undefined' ) )
							ret.pages.push ( $.extend ( {} , v ) ) ;
					} ) ;
					
				} else {
					self.ts.clog ( status ) ;
				}
			}
		} ) ;
		if ( self.ts.verbose ) self.ts.log ( ret.pages.length + " items kept." ) ;

		return ret ;

	} ,

	this.hasProperty = function ( prop , keep ) {
		return self.hasPropertyOrItemLink ( 'prop' , prop , 'items_with_property' , keep ) ;
	}

	this.hasItemLink = function ( item , keep ) {
		return self.hasPropertyOrItemLink ( 'itemlink' , item , 'items_with_item_link' , keep ) ;
	}
	

	this.getWikidataItems = function () {
		if ( self.language == '' && self.project == 'wikidata' ) {
			self.ts.logError ( "Can't get Wikidata items for Wikidata pages" ) ;
			return ;
		}

		var p = {
			action:'get_items_for_pages' ,
			site:self.getWikidataSite() ,
			titles:[]
		} ;
		$.each ( self.pages , function ( k , v ) { p.titles.push ( self.getFullTitle ( k ) ) } ) ;
		
		var ret = new ToolScriptResult ( self.ts , '' , 'wikidata' ) ;
		if ( self.ts.verbose ) self.ts.log ( "Retrieving Wikidata items for " + p.titles.length + " pages on " + self.getSiteTitle() + "..." ) ;
		$.ajax ( self.ts.api.misc , {
			type : "POST" ,
			async : false ,
			data : p ,
			dataType : 'json' ,
			complete : function ( d , status ) {
				if ( status == 'success' ) {
					$.each ( d.responseJSON.data , function ( k , v ) {
						var np = ret.addPage ( v , 0 ) ;
					} ) ;
				} else {
					self.ts.clog ( status ) ;
				}
			}
		} ) ;
		if ( self.ts.verbose ) self.ts.log ( ret.pages.length + " items retrieved." ) ;

		return ret ;
	}
	
	this.createWikidataItems = function () {
		var ret = new ToolScriptResult ( self.ts , '' , 'wikidata' ) ;

		self.ts.logError ( "Creation function deactivated due to abuse" ) ;
		return ret ;
		
		if ( self.language == '' && self.project == 'wikidata' ) {
			self.ts.logError ( "Can't create Wikidata items for Wikidata pages" ) ;
			return ;
		}

		var site = self.getWikidataSite() ;
		$.each ( self.pages , function ( k , v ) {
			var title = self.getFullTitle ( k ) ;
			var p = {
				action:'create_item_from_page',
				botmode:1,
				site:site,
				page:title
			} ;
			$.ajax ( self.ts.api.widar , {
				type : "GET" ,
				async : false ,
				data : p ,
				dataType : 'json' ,
				complete : function ( d , status ) {
					if ( status == 'success' ) {
						if ( d.responseJSON.error == 'OK' ) {
							var q = d.responseJSON.q ;
							ret.addPage ( q , 0 ) ;
							self.ts.log ( "New Wikidata item for " + self.getLink(k) + " is " + ret.getLink(ret.pages.length-1) ) ;
						} else {
							self.ts.logError ( "Problem creating item for " + self.getLink(k) + ": " + d.responseJSON.error ) ;
						}
					} else {
						self.ts.logError ( status ) ;
					}
				}
			} ) ;
		} ) ;
		return ret ;
	}
	
	
	this.addStatementItemlink = function ( prop , target ) {
		if ( self.language != '' || self.project != 'wikidata' ) {
			self.ts.logError ( "Can't add Wikidata statements to " + self.getSiteTitle() ) ;
			return ;
		}
		prop = (prop+'').replace(/\D/g,'') ;
		target = (target+'').replace(/\D/g,'') ;
		$.each ( self.pages , function ( k , v ) {
			if ( v.page_namespace != 0 ) {
				self.ts.logError ( self.getLink(k) + " is not an item, skipping." ) ;
				return ;
			}
			var p = {
				action:'set_claims',
				ids:v.page_title,
				prop:prop,
				target:target,
				botmode:1
			} ;
			$.ajax ( self.ts.api.widar , {
				type : "GET" ,
				async : false ,
				data : p ,
				dataType : 'json' ,
				complete : function ( d , status ) {
					if ( status == 'success' ) {
						if ( d.responseJSON.error == 'OK' ) {
							self.ts.log ( "Added P"+prop+"&rArr;Q"+target+" to " + self.getLink(k) ) ;
						} else {
							self.ts.logError ( "Problem adding statement for " + self.getLink(k) + ": " + d.responseJSON.error ) ;
						}
					} else {
						self.ts.logError ( status ) ;
					}
				}
			} ) ;
		} ) ;
		return self ; // For chaining
	}


// 			var url = "http://tools.wmflabs.org/widar/index.php?action=set_claims&ids="+ids.join(",")+"&prop="+prop+"&target="+target ;

	
	this.splitNamespace = function ( title ) {
		var ret = { page_title:title , page_namespace:0 } ;
		if ( title.indexOf(':') == -1 ) return ret ; // No namespace possible, return default
		
		self.ts.cacheNamespaces ( self ) ;
		var xtitle = title.toLowerCase().replace(/_/g,' ') ;
		
		function titleStartsWith ( s ) {
			if ( typeof s == 'undefined' ) return false ;
			var t = xtitle.slice(0,s.length+1) ;
			return ( t == s.toLowerCase()+':' ) ;
		}
		
		var found = false ;
		var ns = self.ts.namespaces[self.getServer()] ;
		if ( typeof ns == 'undefined' || ns == null ) return ret ;
		$.each ( ns.namespaces , function ( k , v ) {
			if ( titleStartsWith ( v['*'] ) ) {
				found = true ;
				ret.page_title = title.slice ( v['*'].length+1 ) ;
				ret.page_namespace = v.id ;
				return false ;
			} else if ( titleStartsWith ( v['canonical'] ) ) {
				found = true ;
				ret.page_title = title.slice ( v['canonical'].length+1 ) ;
				ret.page_namespace = v.id ;
				return false ;
			}
		} ) ;
		if ( found ) return ret ;

		$.each ( (ns.namespacealiases||[]) , function ( k , v ) {
			if ( titleStartsWith ( v['*'] ) ) {
				ret.page_title = title.slice ( v['*'].length+1 ) ;
				ret.page_namespace = v.id ;
				return false ;
			}
		} ) ;
		
		return ret ;
	}
	
	this.hasPage = function ( p ) {
		var ret = false ;
		$.each ( self.pages , function ( k , v ) {
			if ( v.page_namespace != p.page_namespace ) return ;
			if ( v.page_title != p.page_title ) return ;
			ret = true ;
			return false ;
		} ) ;
		return ret ;
	}
	
	this.filter = function ( o , keep ) {
		if ( typeof o['inList'] != 'undefined' ) {
			if ( self.language != o.inList.language || self.project != o.inList.project ) {
				self.ts.logError ( "filter: base list is from " + self.getSiteTitle() + " but the inList is from " + o.inList.getSiteTitle() ) ;
				return ;
			}
		}
		
		var other_list = {} ;
		if ( typeof o['inList'] != 'undefined' ) {
			$.each ( o['inList'].pages , function ( k , v ) {
				other_list[o['inList'].getFullTitle(k)] = 1 ;
			} ) ;
		}

		var ret = new ToolScriptResult ( self.ts , self.language , self.project ) ;
		$.each ( self.pages , function ( k , v ) {
			var matches = false ;
		
			if ( typeof o['inList'] != 'undefined' ) {
				matches = (typeof other_list[self.getFullTitle(k)] != 'undefined') ;
//				matches = o.inList.hasPage ( v ) ;
			}
			
			if ( typeof o['custom'] != 'undefined' ) {
				if ( o.custom ( v ) ) matches = true ;
			}
			
			if ( ( keep && matches ) || ( !keep && !matches ) ) {
				ret.pages.push ( $.extend ( {} , v ) ) ;
			}
		
		} ) ;
		
		self.ts.log ( "Filter removes " + (self.pages.length-ret.pages.length) + ", keeps " + (ret.pages.length) + " pages" ) ;
		
		return ret ;
	}
	
	this.filterKeep = function ( o ) {
		return self.filter ( o , true ) ;
	}
	
	this.filterRemove = function ( o ) {
		return self.filter ( o , false ) ;
	}
	
	this.getServer = function () {
		return (self.language==''?'www':self.language) + "." + self.project + ".org" ;
	}
	
	this.getTitle = function ( id ) {
		if ( id < 0 || id >= self.pages.length ) { console.log ( "OUT-OF-INDEX: "+id ) ; return ; }
		return self.pages[id].page_title.replace(/_/g,' ') ;
	}

	this.getFullTitle = function ( id ) {
		if ( id < 0 || id >= self.pages.length ) { console.log ( "OUT-OF-INDEX: "+id ) ; return ; }
		var ret = self.pages[id].page_title ;
		if ( self.pages[id].page_namespace != 0 ) {
			self.ts.cacheNamespaces ( self ) ;
			ret = self.ts.namespaces[self.getServer()].namespaces[self.pages[id].page_namespace]['*'] + ':' + ret ;
		}
		return ret.replace(/_/g,' ') ;
	}

	this.getURL = function ( id ) {
		if ( id < 0 || id >= self.pages.length ) { console.log ( "OUT-OF-INDEX: "+id ) ; return ; }
		return "//" + self.getServer() + "/wiki/" + escape(self.getFullTitle(id).replace(/ /g,'_')) ;
	}
	
	this.getLink = function ( id ) {
		if ( id < 0 || id >= self.pages.length ) { console.log ( "OUT-OF-INDEX: "+id ) ; return ; }
		var p = self.pages[id] ;
		var is_pq = self.project == 'wikidata' && (p.page_namespace==0 || p.page_namespace==120) ;
		var ret = "<a target='_blank' href='" + self.getURL(id) + "'" ;
		if ( is_pq ) ret += " title='" + p.page_title + "' class='toolscriptresult_wikidatalink' q='" + p.page_title + "' ns='" + p.page_namespace + "'" ;
		ret += ">" + self.ts.htmlEncode(self.getFullTitle(id)) + "</a>" ;
		if ( is_pq && p.page_namespace==0 ) ret += "&nbsp;<a target='_blank' href='/reasonator/?q=" + p.page_title + "'><img src='" + self.ts.icon.reasonator + "' border=0 title='Reasonator' /></a>" ;
		return ret ;
	}
	
	this.getSiteTitle = function () {
		if ( self.language == '' ) return self.project ;
		return self.language + "." + self.project ;
	}
	
	this.getWikidataSite = function () {
		if ( self.project == 'wikipedia' ) return self.language + 'wiki' ;
		return self.language + self.project ;
	}
	
	this.initFromWikidataSite = function ( site ) {
		var m = site.match ( /^(.+)wiki$/ ) ;
		if ( m == null ) m = site.match ( /^(.+)(wik.+)$/ ) ;
		else m[2] = 'wikipedia' ;
		self.language = m[1] ;
		self.project = m[2] ;
	}

	this.getItemsWithoutSitelink = function ( site ) {
		if ( self.project != 'wikidata' ) return self.ts.logError ( "getItemsWithoutSitelink() for " + self.language + "." + self.project ) ;
		if ( typeof site == 'undefined' ) return self.ts.logError ( "getItemsWithoutSitelink() with no site parameter" ) ;
		var p = {
			action:'items_no_site' ,
			site : site ,
			items : []
		} ;
		$.each ( self.pages , function ( k , v ) {
			if ( v.page_namespace != 0 ) return ;
			p.items.push ( v.page_title ) ;
		} ) ;
		var ret = new ToolScriptResult ( self.ts , '' , 'wikidata' ) ;
		self.ts.log ( "Checking " + p.items.length + " Wikidata items items for missing sitelinks to <i>" + site + "</i>..." ) ;
		$.ajax ( self.ts.api.misc , {
			type : "POST" ,
			async : false ,
			data : p ,
			dataType : 'json' ,
			complete : function ( d , status ) {
				if ( status == 'success' ) {
					$.each ( d.responseJSON.data , function ( k , v ) {
						ret.pages.push ( { page_title:v.replace(/ /g,'_') , page_namespace:0 } ) ;
					} ) ;
				} else {
					self.ts.clog ( status ) ;
				}
			}
		} ) ;
		self.ts.log ( ret.pages.length + " pages retrieved." ) ;
		return ret ;
	}
	
	this.getSitelinks = function ( site ) {
		if ( self.project != 'wikidata' ) return self.ts.logError ( "getSitelinks() for " + self.language + "." + self.project ) ;
		if ( typeof site == 'undefined' ) return self.ts.logError ( "getSitelinks() with no site parameter" ) ;
		var p = {
			action:'items2site' ,
			site : site ,
			items : []
		} ;
		$.each ( self.pages , function ( k , v ) {
			if ( v.page_namespace != 0 ) return ;
			p.items.push ( v.page_title ) ;
		} ) ;
		var ret = new ToolScriptResult ( self.ts ) ;
		ret.initFromWikidataSite ( site ) ;
		self.ts.log ( "Retrieving pages for " + p.items.length + " Wikidata items on " + site + ", aka " + ret.getSiteTitle() + "..." ) ;
		$.ajax ( self.ts.api.misc , {
			type : "POST" ,
			async : false ,
			data : p ,
			dataType : 'json' ,
			complete : function ( d , status ) {
//				console.log ( d.responseJSON.data ) ;
				if ( status == 'success' ) {
					$.each ( d.responseJSON.data , function ( k , v ) {
						var np = ret.splitNamespace ( v[0].replace(/ /g,'_') ) ;
						np.q = v[1] ;
						ret.pages.push ( np ) ;
					} ) ;
				} else {
					self.ts.clog ( status ) ;
				}
			}
		} ) ;
		self.ts.log ( ret.pages.length + " pages retrieved." ) ;
		return ret ;
	}
	
	
	this.mergeAndDelete = function ( q_from , q_to ) {
		if ( self.project != 'wikidata' ) return self.ts.logError ( "getSitelinks() for " + self.language + "." + self.project ) ;

		// Merge
		var p = {
			action:'merge_items',
			botmode:1,
			from:q_from,
			to:q_to
		} ;
		$.ajax ( self.ts.api.widar , {
			type : "GET" ,
			async : false ,
			data : p ,
			dataType : 'json' ,
			complete : function ( d , status ) {
				if ( status == 'success' ) {
					ts.log ( '# [[' + q_from + ']] {{rfd links|'+q_from+'|Merged with [['+q_to+']]}} Merged with [['+q_to+']]' ) ;
//					self.ts.log ( q_from + " => " q_to ) ;
				} else {
				}
			}
		} )

/*
		$.getJSON ( wdgame.widar_api , {
			action:'merge_items',
			from:'Q'+data.item2,
			to:'Q'+data.item1,
			botmode:1
		} , function ( d ) {
			$('#run_status div[id='+data.id+']').html ( "Requesting deletion for Q"+data.item2 ) ;
			var row = "{{subst:Rfd|1=Q"+data.item2+"|2=Merged into [[Q"+data.item1+"]]}} --~~~~" ;
			$.getJSON ( wdgame.widar_api , {
				action:'add_row',
				page:'Wikidata:Requests for deletions',
				row:row,
				botmode:1
			} , function ( d ) {
				self.setItemStatus(data,'SAME') ;
			} ) ;
		} ) ;*/
	}
	
	this.show = function ( offset ) {
		if ( typeof self.ts.list_selector == 'undefined' ) return self.ts.logError ( "showResultList(): list_selector not defined" ) ;
		if ( typeof offset == 'undefined' ) offset = 0 ;
		var size = 50 ;
		var h = "" ;
		
		function getNavbar ( top ) {
			if ( self.pages.length <= size ) return "" ; // No navbars needed
			var h2 = [] ;
			for ( var i = 0 ; i < self.pages.length ; i += size ) {
				var s = (i+1) + "&ndash;" + (i+size) ;
				if ( i == offset ) h2.push ( "<b>" + s + "</b>" ) ;
				else h2.push ( "<a href='#' class='toolscript_list_navbar_link' offset=" + i + ">" + s + "</a>" ) ;
			}
			return "<div style='max-height:60px;overflow:auto;border-" + (top?'bottom':'top') + ":1px solid #EEE'>" + h2.join ( " | " ) + "</div>" ;
		}

		var has_q = false ;
		if ( self.project != 'wikidata' ) {
			for ( var i = 0 ; i < size && i+offset < self.pages.length ; i++ ) {
				if ( typeof self.pages[i+offset].q != 'undefined' ) has_q = true ;
			}
		}
		
		h += "<div class='output_list'>" ;
		h += "<div style='text-align:center;font-weight:bold'>Showing " + self.pages.length + " results from " + self.getSiteTitle() + "</div>" ;
		h += getNavbar ( true ) ;
		h += "<table class='table-condensed table-striped'>" ;
		h += "<thead><tr><th>#</th><th>Page</th>" ;
		if ( has_q ) h += "<th>Wikidata item</th>" ;
		h += "</tr></thead>" ;
		h += "<tbody>" ;
		for ( var i = 0 ; i < size && i+offset < self.pages.length ; i++ ) {
			var p = offset + i ;
			h += "<tr>" ;
			h += "<td>" + (p+1) + "</td>" ;
			h += "<td>" + self.getLink(p) + "</td>" ;
			if ( has_q && typeof self.pages[p].q != 'undefined' ) {
				var q = self.pages[p].q ;
				h += "<td>" ;
				h += "<a target='_blank' href='//www.wikidata.org/wiki/" + q + "'>" + q + "</a>" ;
				h += "&nbsp;<a target='_blank' href='/reasonator/?q=" + q + "'><img src='" + self.ts.icon.reasonator + "' border=0 title='Reasonator' /></a>" ;
				h += "</td>" ;
			} else if ( has_q ) h += "<td/>" ;
			if ( typeof self.pages[p].note != 'undefined' ) h += "<td>" + self.pages[p].note + "</td>" ;
			h += "</tr>" ;
		}
		h += "</tbody></table>" ;
		h += getNavbar ( false ) ;
		h += "</div>" ;
		$(self.ts.list_selector).html ( h ) ;
		$(self.ts.list_selector).find('a.toolscript_list_navbar_link').each ( function () {
			var a = $(this) ;
			a.click ( function () {
				self.show ( a.attr('offset')*1 ) ;
				return false ;
			} ) ;
		} ) ;
		$('a[href="'+self.ts.list_selector+'"]').tab('show');
		self.ts.labelWikidataLinks() ;
	}

	// Single-parameter site resolver
	if ( typeof lang__ != 'undefined' && typeof proj__ == 'undefined' ) {
		if ( lang__ == 'wikidata' ) {
			self.language = '' ;
			self.project = 'wikidata' ;
		} else {
			self.initFromWikidataSite ( lang__ ) ;
		}
	}
	
} // END OF ToolScriptResult







function ToolScript () {

	var self = this ;
	
	
	this.verbose = true ;
	
	this.languages = [ 'en','de','es','it','fr','nl' ] ;
	
	this.api = {
		misc:'./misc.php',
		proxy:'./proxy.php',
		wdq:'./wdq_proxy.php',
		widar:'/widar/index.php',
		categorytree:'/quick-intersection/index.php?' ,
		wikidata:'//www.wikidata.org/w/api.php?callback=?'
	}
	
	this.icon = {
		reasonator:'//upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Reasonator_logo_proposal.png/16px-Reasonator_logo_proposal.png'
	}
	
	this.wikidatacache = {}
	this.namespaces = {} ;

	this.log = function ( s ) {
		if ( typeof self.output_selector == 'undefined' ) return ;
		var out = $(self.output_selector) ;
		out.append ( "<div class='output_row'>" + s + "</div>" ) ;
//		setTimeout ( function () {
//			out.animate({ scrollTop: out.height() }, "fast");
//		} , 1 ) ;
	}
	
	this.logConsole = function ( s ) {
		self.log ( "<span class='console'>Console: </span>" + s ) ;
	}

	this.logError = function ( s ) {
		self.log ( "<span class='error'>Error: </span>" + s ) ;
	}

	this.logAction = function ( s ) {
		if ( self.verbose ) self.log ( s ) ;
	}
	
	this.getNewList = function ( language , project ) {
		return new ToolScriptResult ( self , language , project ) ;
	}
	
	
	this.wdq = function ( params ) {
		if ( typeof params == 'string' ) params = { query:params } ;
		self.logAction ( "Running WDQ query <i>" + params.query + "</i> ..." ) ;

		var p = { q:params.query } ;
		var ret = new ToolScriptResult ( self , '' , 'wikidata' ) ;
		$.ajax ( self.api.wdq , {
			type : "POST" ,
			async : false ,
			data : p ,
			dataType : 'json' ,
			complete : function ( d , status ) {
				if ( status == 'success' ) {
					$.each ( d.responseJSON.items , function ( k , v ) {
						ret.pages.push ( { page_title:'Q'+v , page_namespace:0 } ) ;
					} ) ;
					if ( self.verbose ) self.log ( "Query successful, " + ret.pages.length + " results." ) ;
				} else {
					self.logError ( "Query unsuccessful: " + status ) ;
				}
			}
		} ) ; 
		
		return ret ;
	}

	
	this.categorytree = function ( params ) {
		var p = { project:'wikipedia' , depth:12 , format:'json' , max:30000 } ;
		$.each ( params , function ( k , v ) {
			if ( $.isArray(v) ) v = v.join("\n") ;
			if ( k == 'wikidata' ) {
				p.wikidata = 1 ;
				if ( v == 'noitem' ) p.wikidata_no_item = 1 ;
			} else if ( k == 'root' ) {
				p.cats = v ;
			} else if ( k == 'language' ) {
				p.lang = v ;
			} else {
				p[k] = v ;
			}
		} ) ;
		p.sparse = 1 ;
		
		self.logAction ( "Starting categorytree query..." ) ;
		
		var ret = new ToolScriptResult ( self , p.lang , p.project ) ;
		$.ajax ( self.api.categorytree , {
			type : "POST" ,
			async : false ,
			data : p ,
			dataType : 'json' ,
			complete : function ( d , status ) {
				if ( status == 'success' ) {
					$.each ( d.responseJSON.pages , function ( k , v ) {
						ret.pages.push ( ret.splitNamespace ( v ) ) ;
					} ) ;
					if ( self.verbose ) self.log ( "Query successful, " + ret.pages.length + " results." ) ;
				} else {
					// TODO error handling
					self.logError ( "Query unsuccessful: " + status ) ;
				}
			}
		} ) ; 
		
		return ret ;
	}
	
	
	
	
	this.htmlEncode = function(value){
		if (value) {
			return jQuery('<div />').text(value).html();
		} else {
			return '';
		}
	}
 
	this.htmlDecode = function(value) {
		if (value) {
			return $('<div />').html(value).text();
		} else {
			return '';
		}
	}
	

	this.labelWikidataLinks = function () { // TODO this only works for 50 labels; should be enough for everyone, but...
		var qs = [] ;
		$('a.toolscriptresult_wikidatalink').each ( function () {
			var a = $(this) ;
			var q = a.attr('q') ;
			if ( typeof self.wikidatacache[q] == 'undefined' ) qs.push ( q ) ;
		} ) ;
		
		function showLabels () {
			$('a.toolscriptresult_wikidatalink').each ( function () {
				var a = $(this) ;
				var q = a.attr('q') ;
				if ( typeof self.wikidatacache[q] == 'undefined' ) return ; // No label loaded
				var label = q ;
				$.each ( self.languages , function ( dummy , l ) {
					if ( typeof (self.wikidatacache[q].labels||{})[l] == 'undefined' ) return ;
					label = self.wikidatacache[q].labels[l].value ;
					return false ;
				} ) ;
				a.text ( label ) ;
			} ) ;
		}

		if ( qs.length > 0 ) {
			$.getJSON ( self.api.wikidata , {
				action:'wbgetentities',
				ids:qs.join('|'),
				format:'json',
				props:'labels'
			} , function ( d ) {
				$.each ( (d.entities||{}) , function ( k , v ) {
					self.wikidatacache[k] = v ;
				} ) ;
				showLabels() ;
			} ) ;
		} else {
			showLabels() ;
		}

	}
	
	this.show = function ( o ) {
		if ( typeof o == 'object' && o.classname == 'ToolScriptResult' ) {
			return o.show() ;
		}
		self.logError ( "show(): Unknown type of object " + JSON.stringify(o) ) ;
	}
	
	this.microtime = function () {
		var unixtime_ms = new Date().getTime();
		return (unixtime_ms/1000);
	}
	
	this.delay = function ( sec ) {
		var start = self.microtime() ;
		while ( self.microtime()-start < sec ) {}
	}
	
	this.cacheNamespaces = function ( o ) {
		var server = o.getServer() ;
		if ( typeof self.namespaces[server] != 'undefined' ) return ; // Done that

		self.delay ( 0.3 ) ;
		$.ajax ( self.api.proxy , {
			type : "GET" ,
			async : false ,
			cache : false ,
			data : { url:encodeURI('http://'+server+'/w/api.php?action=query&meta=siteinfo&siprop=namespaces|namespacealiases&format=json') } ,
			dataType : 'json' ,
			complete : function ( d , status ) { // TODO error check
				if ( typeof d.responseJSON == 'undefined' ) return ;
				self.namespaces[server] = d.responseJSON.query ;
			}
		} ) ;
	}
	
	
//this.debug = true ;	
	this.run = function ( code ) {
		var use_clog = true ;
//		$.ajaxSetup ({ cache: false });
		self.log ( "<div class='run_started'>RUN STARTED</div>" ) ;
		if ( use_clog ) self.clog = console.log ; // clog is now "real" console.log
		if ( use_clog ) console.log = self.logConsole ;
		
		if ( self.debug ) {
			eval ( "function toolscript_run(){\n"+code+"\n};toolscript_run();" ) ;
		} else {
			try {
				eval ( "function toolscript_run(){\n"+code+"\n};toolscript_run();" ) ;
			} catch ( e ) {
				self.logError ( e.message ) ;
			}
		}
		if ( use_clog ) console.log = self.clog ;
		self.log ( "<div class='run_complete'>RUN COMPLETE</div>" ) ;
//		$.ajaxSetup ({ cache: true });
	}

} ;
